\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{easyfig}
\usepackage[labelformat=empty]{caption}

\title{Using Git}
\author{Taylor D. Scott}
\date{July 10, 2017}

\usetheme{boadilla}
\usecolortheme{beaver}

\begin{document}

\frame{\titlepage}

\begin{frame}[fragile]
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\section{Version control}
\subsection{What is it, and why should I use it?}
\begin{frame}
\frametitle{Version control}
\begin{columns}

\column{0.55\textwidth}
\begin{itemize}

\item Version control automatically keeps a record of your project
\item Complete history of project
\item Ability to revert changes
\item Collaboration is easy
\item Entire history can be on multiple machines

\Figure[width=0.8\textwidth]{images/git_motivation_tweet.png}
\end{itemize}
\column{0.45\textwidth}
\Figure[caption={\tiny\raggedleft PhD Comics},width=\textwidth, height=0.70\textheight, keepaspectratio]{images/phd_comics_phd101212s.png}

\end{columns}

\end{frame}

\subsection{Git for version control}
\begin{frame}
\frametitle{Why should I use Git for version control?}

\begin{itemize}
\item One of many version control systems (VCS)
\begin{itemize}
\item Others include Hg (mercurial) and SVN
\end{itemize}
\item Most widely used VCS
\begin{itemize}
\item 2015 Stack Overflow Developer Survey
\end{itemize}
\item Free online repositories for collaboration, back-ups
\begin{itemize}
\item GitHub, Bitbucket, GitLabs, etc.
\end{itemize}
\item Easy installation on Windows, Linux, and Mac
\item \url{https://git-scm.com/}
\end{itemize}
\Figure[caption={\tiny\raggedleft Source: git-scm.com}, width=0.9\textwidth]{images/companies-using-git.png}
\end{frame}

\section{Basic usage}
\subsection{Overview}
\begin{frame}
\frametitle{Git overview}
\begin{columns}
\column{0.6\textwidth}
\begin{itemize}
\item The git workflow consists of 3 `spaces'
\begin{itemize}
\item \emph{Working directory}: The current state of your project. If you open a file, you see the version in your working directory
\item \emph{Staging area} or \emph{Index}: Changes to your files that are ready to be committed (saved to the repository)
\item \emph{Repository}: The complete history of your project
\end{itemize}
\item Changes are \emph{added} to the staging area and \emph{committed} to the repository
\end{itemize}
\column{0.4\textwidth}
\Figure[caption={\tiny\raggedleft newtFire, \emph{Explain Git Shell}}, width=0.95\textwidth, height=0.8\textheight, keepaspectratio]{images/git-add-commit.png}
\end{columns}
\end{frame}

\subsection{Installation}
\begin{frame}
\frametitle{Installation}
\begin{itemize}
\item Packaged installers for Windows, Mac, and Linux
\item Lots of guides online
\item {\tiny\url{https://git-scm.com/book/en/v1/Getting-Started-Installing-Git}}
\item Configure git with your name and email
\end{itemize}
\vspace{0.25in}
\begin{block}{Command}
\texttt{git config --global user.name "<your name>"}\\
\texttt{git config --glboal user.email "<email>"}
\end{block}
\end{frame}

\subsection{Initializing a repository}
\begin{frame}
\frametitle{Initializng a repository}
\begin{block}{Command}
Use \texttt{git init} to initialize a repository
\end{block}
\vspace{0.25in}
\begin{itemize}
\item Starting a repository is called initializing
\item Your project should be contained in a folder
\item From the command line, navigate to your folder and run \texttt{git init} to initialize a repository
\item Do NOT nest repositories. It will cause the outermost repository to explode in size
\end{itemize}
\end{frame}

\subsection{Saving changes}
\begin{frame}
\frametitle{Saving changes}
\begin{block}{Command}
2 step process:
\begin{enumerate}
\item Stage your changes with \texttt{git add <file>}
\item Commit your changes with \texttt{git commit -m "<message>"}
\end{enumerate}

\end{block}
\vspace{0.25in}
\begin{itemize}
\item Be sure to add \emph{all} the files you want to commit
\item Leave a helpful message with your commit explaining what changes you made and why
\item Commit often -- it makes it easier to undo unwanted changes
\item Use \texttt{git status} to see where you are in the commit process
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The commit process}
\Figure[width=0.9\textwidth, height=0.85\textheight, keepaspectratio]{images/git-commit-process.png}
\end{frame}

\subsection{Ignoring files}
\begin{frame}
\frametitle{Ignoring file with \texttt{.gitignore}}
There are some files you \emph{don't} want to commit to your repository
\begin{itemize}
\item Large binary files -- executables, large images, large pdfs, etc.
\item Sensitive information -- passwords, confidential data, etc.
\item Machine specific configuration -- anything that's only for your computer
\end{itemize}
You can tell git which files to ignore with a \texttt{.gitignore} file
\begin{enumerate}
\item Create an empty file called \texttt{.gitignore}
\begin{itemize}
\item Type the file name exactly, including the dot and with no extension
\end{itemize}
\item Add the files you want to ignore, each on a separate line
\begin{itemize}
\item You can ignore folders by adding a ``/" to the end of the folder name
\item You can use wildcards, e.g., \texttt{*.pdf} to ignore all pdfs
\end{itemize}
\item Stage and commit your .gitignore file to your repository
\end{enumerate}
\end{frame}

\subsection{Viewing your history}
\begin{frame}
\frametitle{View a log of all commits}

\begin{block}{Command}
\texttt{git log} to view a detailed log\\
\texttt{git log --oneline} to view an abbreviated log (hash + message)\\
\texttt{git log -n <n>} to view the last \emph{n} commits
\end{block}
\vspace{0.25in}
\begin{itemize}
\item \texttt{git log} displays the author, email, date, comment, and a hash for each commit
\item Each commit has an associated \emph{hash}, a unique string of characters which can be used to reference a commit
\item You can refer to a commit by the full hash, or by the first 7 characters
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{\texttt{git log}}
\Figure[width=0.9\textwidth, height=0.9\textheight, keepaspectratio]{images/git-log-full.png}
\end{frame}

\begin{frame}
\frametitle{\texttt{git log --oneline}}
\Figure[width=0.9\textwidth, height=0.9\textheight, keepaspectratio]{images/git-log-oneline.png}
\end{frame}

\section{Remote repositories}

\subsection{Overview}
\begin{frame}
\frametitle{Overview}
\begin{itemize}
\item You can host your repository in the cloud
\item This lets other collaborate on your project
\item It also keeps a back up of your repository
\item Popular services are GitHub, Bitbucket, and GitLab
\begin{itemize}
\item GitHub is the standard and most widely used
\item I prefer bitbucket, because it lets me have private repositories
\end{itemize}
\end{itemize}
\Figure[caption={\tiny\raggedleft \emph{Medium}, GitHub vs. Bitbucket vs. GitLab vs. Coding}, width=0.9\textwidth, height=0.4\textheight, keepaspectratio]{images/remote-repository-logos.png}
\end{frame}

\subsection{Syncing with a remote repository}
\begin{frame}
\frametitle{Create a remote repository}
\begin{block}{Command}
\texttt{git remote add origin <URL>}\\
\emph{\small The URL will look like:}\\
\texttt{\small https://username@bitbucket.org/username/your-repository.git}
\end{block}
\begin{enumerate}
\item Create a repository on GitHub, Bitbucket, etc.
\item Initialize a repository on your computer (or navigate to an existing one)
\item Add a \emph{remote source} to your local repository
\end{enumerate}

\emph{The popular remote hosting services have step-by-step guides for setting up a remote repository}
\end{frame}

\begin{frame}
\frametitle{Syncing with the remote repository}
\begin{block}{Command}
\texttt{git pull origin master}\\
\end{block}
You \emph{pull} changes from the remote repository to keep your local repository
\vspace{0.5in}
\begin{block}{Command}
\texttt{git push origin master}
\end{block}
You \emph{push} changes from your local repository to the remote repository
\end{frame}

\subsection{Forks and Pull Requests}
\begin{frame}
\frametitle{Forks and pull requests -- collaborating with others}
You can collaborate with others through \emph{forks} and \emph{pull requests}.
\begin{itemize}
\item \emph{Forking} makes a copy of someone else's repository
\item A \emph{pull request} sends your changes to the original repository owner for approval
\end{itemize}

\Figure[caption={\tiny\raggedleft \emph{Data School}, A simple guide to forks in GitHub and Git}, height=0.5\textheight, width=0.8\textwidth, keepaspectratio]{images/fork-pull-request.png}
\end{frame}
\end{document}